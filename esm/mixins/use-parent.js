import { Vue } from '../vue'; // --- Mixin ---
// @vue/component

export var useParentMixin = Vue.extend({
  computed: {
    bvParent: function bvParent() {
      return this.$parent || this.$root === this && this.$options.bvParent;
    }
  }
});