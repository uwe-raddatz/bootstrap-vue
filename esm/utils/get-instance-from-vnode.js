import { isVue3 } from '../vue';
export var getInstanceFromVNode = function getInstanceFromVNode(vnode) {
  return isVue3 ? vnode.__vueParentComponent.ctx : vnode.__vue__;
};